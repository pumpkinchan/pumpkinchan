###Stuff that needs done first:
* Set up db (repo created but that's it so far)
* Create boards   DONE
* Allow users to post on board
* User-created threads are posted on board
* Get sockets/channels working with threads
* Post deletion




###Later stuff:
* Add extra media posts (images, webms, etc.)
* Recent posts
* rules page
* stats/stats page
* quick reply
* spoilered text/images
* expand/remove all images in threads/board
* reporting
* banning
* banners
* hide/unhide threads
* flood detection
* thread search/filter for boards
* archive? 