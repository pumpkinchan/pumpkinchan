defmodule Pumpkinchan.ThreadController do
	use Pumpkinchan.Web, :controller
  alias Pumpkinchan.Thread
	
  def create(conn, %{"post" => post_params}, board) do
		# create thread and send it to db
		changeset =
			board
		  |> build_assoc(:threads)
  	  |> Thread.changeset(post_params)
		case Repo.insert(changeset) do
			{:ok, thread} ->
				conn 
				|> put_flash(:info, "thread created!")
				|> redirect(to: board_thread_path(conn, :show, board, thread.id))
			
			{:error, changeset} ->
				render(conn, "index.html", changeset: changeset)
		end
	end

	def show(conn, %{"id" => id}) do
	 thread = Repo.get_by(Thread, slug: id)	
   posts = Repo.all(thread_posts(thread), :posts)
		render(conn, "show.html", posts: posts)
	end
	
	defp thread_posts(thread) do
		assoc(thread, :posts)
	end
end
