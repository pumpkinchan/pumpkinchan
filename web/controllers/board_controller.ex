defmodule Pumpkinchan.BoardController do
	use Pumpkinchan.Web, :controller
	alias Pumpkinchan.Board
	alias Pumpkinchan.Post

	def index(conn, %{"id" => id}) do
		board = Repo.get_by(Board, slug: id)
		changeset = Post.changeset(%Post{})
		render conn, "index.html", board: board, changeset: changeset
		#render conn, "index.html", board: board
	end
end
