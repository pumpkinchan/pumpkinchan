defmodule Pumpkinchan.Thread do
  use Pumpkinchan.Web, :model

  schema "threads" do
    belongs_to :boards, Pumpkinchan.Board
		has_many :posts, Pumpkinchan.Post
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end
