defmodule Pumpkinchan.Post do
  use Pumpkinchan.Web, :model

  schema "posts" do
    field :content, :string

		belongs_to :threads, Pumpkinchan.Thread
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:content])
    |> validate_required([:content])
  end
end
