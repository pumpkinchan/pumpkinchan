defmodule Pumpkinchan.Board do
  use Pumpkinchan.Web, :model

  schema "boards" do
		field :name, :string
		field :slug, :string
		
    has_many :threads, Pumpkinchan.Thread
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ :empty) do
    struct
    |> cast(params, [])
    |> validate_required([])
  end
end

defimpl Phoenix.Param, for: Pumpkinchan.Board do
	def to_param(%{slug: slug, id: id}) do
		"#{slug}"
	end
end
