defmodule Pumpkinchan.Router do
  use Pumpkinchan.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Pumpkinchan do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
		get "/:id", BoardController, :index, param: "id"
		resources "/:id", BoardController do
			get "/thread", ThreadController, :index
			get "/thread/:id", ThreadController, :show, param: "id"
		end
	end

  # Other scopes may use custom stacks.
  # scope "/api", Pumpkinchan do
  #   pipe_through :api
  # end
end
