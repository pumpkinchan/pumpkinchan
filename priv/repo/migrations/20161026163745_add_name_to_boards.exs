defmodule Pumpkinchan.Repo.Migrations.AddNameToBoards do
  use Ecto.Migration

  def change do
		alter table(:boards) do
			add :name, :string
			add :slug, :string
		end
  end
end
