defmodule Pumpkinchan.Repo.Migrations.CreatePost do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :content, :string
			add :thread_id, references(:thread, on_delete: :nothing)

      timestamps()
    end

  end
end
