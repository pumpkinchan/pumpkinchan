defmodule Pumpkinchan.Repo.Migrations.CreateThread do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :board_id, references(:boards, on_delete: :nothing)

      timestamps()
    end
    create index(:threads, [:board_id])
  end
end
