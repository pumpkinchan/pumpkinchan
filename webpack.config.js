'use strict';

var path = require("path");

var SRC_DIR = "./web/static";
var JS_SRC_DIR = SRC_DIR + "/js";

var DEST_DIR = "./priv/static";

module.exports = {
    module: {
	loaders: [
	    {
		test: /\.(js?)$/,
		exclude: /node_modules/,
		loaders: ["babel"],
		include: path.resolve(JS_SRC_DIR),
	    },
	    {
		test: /\.scss$/,
		loaders: ["style", "css", "autoprefixer", "sass"],
		include: path.resolve(SRC_DIR)
	    }
	]
    },
    
    entry: {
	application: [
	    JS_SRC_DIR + "/index.js",
	    SRC_DIR + "/css/app.scss"
	]
    },
    
    resolve: {
	extensions: ["", ".js", ".scss"],
	modulesDirectories: ["node_modules"]
    },
  
    output: {
	path: DEST_DIR + "/js",
	filename: "app.js"
    }
};
