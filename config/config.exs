# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :pumpkinchan,
  ecto_repos: [Pumpkinchan.Repo]

# Configures the endpoint
config :pumpkinchan, Pumpkinchan.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "jo/I0mHeWx+3RpDMM3RSr7br5IAe7uaS5djWdwFCJy6mMMBxS1hTJ8fMFz1fc2q6",
  render_errors: [view: Pumpkinchan.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Pumpkinchan.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"


# Slime Template Configs
config :phoenix, :template_engines,
slim: PhoenixSlime.Engine,
slime: PhoenixSlime.Engine

config :slime, :keep_lines, true
